# Bit-demo Worker application
This is a server side application for monitoring Bitcoin blocks creation time

## Installation and execution details
### Prerequisites
Technical requirements: JDK 8 and Redis server running locally on default port (6379) 

Redis can be started either as a standalone or in docker:

`docker run --network="host" --name some-redis -d redis`

### Run test
`./mvnw clean test` Will run unit tests only

`./mvnw clean integration-test` Will run unit and integration tests

### Start server
To start server execute following command in directory with project:

`./mvnw spring-boot:run
`
### Start using:
If no errors appeared in console, you can see in logs that server started collecting data

### Configuration:
* **application.properties** is at `src/main/resources/applications.properties`

   `targetListSize` - how many blocks to fetch and save. Should be desired blocks count in UI +1 (see implementation below)
   
   `blockChainCount` - number of blocks to check upon each new block arrival
   
   `blockchain.api.rest`, `blockchain.api.websocket` - API provider URI's


## Implementation details
### Tech stack
Server is written in Java 8 using Spring Framework (Components: Boot, Core, Data, Test)

Redis is used as a distributed data store to exchange data with client applications

Project Reactor is used internally as implementation of Observer pattern. 

Websocket used for notification about new blocks

Testing stack:
* Unit tests are written in Groovy using Spock Framework. It was chosen because of speed in writing new tests and more flexibility
* WireMock is used as a Mock Web server in integration tests to simulate Blockchain API
* Jedis as a Redis client

### Algorithm
To get _Blockchain data_ server uses two external APIs:
* `https://blockchain.info/api/blockchain_api` _Blocks for one day_ - to get Block details history
* `https://blockchain.info/api/api_websocket` _Subscribing to new Blocks_ - For notifications about new blocks

_Block computation time_ can be roughly computed as a difference between current block creation time and next block time

On start server reads desired number of blocks from API (including previous day's if needed). It stores it in Redis so clients can start reading it

When new block arrives via Websocket, server reads `blockChainCount` records from API and checks it accordance to what stored in Redis. 
If that block chain is not found in new data from API, server rebuilds Redis data, otherwise it adds missing elements, removing the eldest

### Storage format
Data is store in redis in following format "`<blockCreationTime>`;`<blockHeight>`" so the client is responsible for calculating block computation time

### Components description 
* `BlockStoreService` responsible for storing blocks in Redis in strict order
* `BlockPublishService` internal Publisher-Subscriber mechanism using _Reactor_. It notifies `BlockStoreService` about new Block
* `BlockClientService` Rest API and Websocket requests encapsulated in this module
* `BlockManagingService` coordinates all work


### Tests
* All components are covered with unit tests. They are located in `src/test/groovy` package
* `BitdemoWorkerIntegrationTests` is an integration test which starts context and simulates data exchange via Rest API, receiving new Block via Websocket and checks result in Redis
