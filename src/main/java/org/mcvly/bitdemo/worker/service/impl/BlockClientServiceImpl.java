package org.mcvly.bitdemo.worker.service.impl;

import org.mcvly.bitdemo.worker.client.BlockchainDataRestClient;
import org.mcvly.bitdemo.worker.client.BlockchainDataWebsocketClient;
import org.mcvly.bitdemo.worker.client.dto.Block;
import org.mcvly.bitdemo.worker.service.BlockClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
public class BlockClientServiceImpl implements BlockClientService {

    private final BlockchainDataRestClient restClient;
    private final BlockchainDataWebsocketClient websocketClient;

    @Autowired
    public BlockClientServiceImpl(BlockchainDataRestClient restClient,
                                  BlockchainDataWebsocketClient websocketClient) {
        this.restClient = restClient;
        this.websocketClient = websocketClient;
    }

    @Override
    public void startPolling() {
        websocketClient.init();
    }

    @Override
    public List<Block> getLastRecordsFromApi(int count) {
        Instant currentTime = Instant.now();
        List<Block> blockHistory = restClient.getBlockHistory(currentTime);
        if (blockHistory.size() < count) {
            blockHistory.addAll(restClient.getBlockHistory(currentTime.minus(1, ChronoUnit.DAYS)));
        }

        return blockHistory.stream()
                .sorted((o1, o2) -> Long.compare(o2.getTime(), o1.getTime()))
                .limit(count).collect(toList());
    }

}
