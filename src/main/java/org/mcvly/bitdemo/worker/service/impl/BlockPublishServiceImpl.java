package org.mcvly.bitdemo.worker.service.impl;

import org.mcvly.bitdemo.worker.client.dto.Block;
import org.mcvly.bitdemo.worker.service.BlockPublishService;
import org.springframework.stereotype.Component;
import reactor.core.publisher.EmitterProcessor;
import reactor.core.publisher.Flux;

@Component
public class BlockPublishServiceImpl implements BlockPublishService {

    private final EmitterProcessor<Block> blockStream;

    public BlockPublishServiceImpl() {
        this.blockStream = EmitterProcessor.create();
    }

    @Override
    public Flux<Block> fetchBlockStream() {
        return blockStream;
    }

    @Override
    public void submitNewBlock(Block block) {
        blockStream.onNext(block);
    }
}
