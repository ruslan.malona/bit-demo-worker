package org.mcvly.bitdemo.worker.service;

import org.mcvly.bitdemo.worker.client.dto.Block;

import java.util.List;

public interface BlockStoreService {

    /**
     * Stores all provided Blocks in Redis, replacing existing ones
     *
     * @param blocks {@link Block}s to store
     */
    void fullRebuild(List<Block> blocks);

    /**
     * Merges existing blocks in Redis with new ones.
     * It tries to find chain of latest {@code blockChainCount} elements from Redis in new blocks. If finds - appends
     * only subsequent, removing eldest, otherwise returns -1
     *
     * @param blocks {@link Block}s to store
     * @return count of elements inserted or -1 if needs full rebuild
     */
    int partialRebuild(List<Block> blocks);
}
