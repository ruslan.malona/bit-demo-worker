package org.mcvly.bitdemo.worker.service;

import org.mcvly.bitdemo.worker.client.dto.Block;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BlockManagingService {

    private static final Logger LOG = LoggerFactory.getLogger(BlockManagingService.class);

    private final BlockClientService blockClientService;
    private final BlockStoreService blockStoreService;
    private final BlockPublishService blockPublishService;
    private final Integer targetListSize;
    private final Integer blockChainCount;

    @Autowired
    public BlockManagingService(BlockClientService blockClientService,
                                BlockStoreService blockStoreService,
                                BlockPublishService blockPublishService,
                                @Value("${targetListSize}") Integer targetListSize,
                                @Value("${blockChainCount}") Integer blockChainCount) {
        this.blockClientService = blockClientService;
        this.blockStoreService = blockStoreService;
        this.blockPublishService = blockPublishService;
        this.targetListSize = targetListSize;
        this.blockChainCount = blockChainCount;
    }

    public void start() {
        List<Block> lastRecordsFromApi = blockClientService.getLastRecordsFromApi(targetListSize);
        blockStoreService.fullRebuild(lastRecordsFromApi);
        blockPublishService.fetchBlockStream()
                .doOnNext(this::newBlockReceived)
                .doOnError(throwable -> LOG.warn("Error in stream", throwable))
                .subscribe();
        blockClientService.startPolling();
    }

    void newBlockReceived(Block block) {
        try {
            LOG.info("New block received {}", block);
            // take twice more elements so we're able to find blockchain from redis even if it's created several blocks ago
            List<Block> blockChainFromApi = blockClientService.getLastRecordsFromApi(blockChainCount * 2);
            addNewBlockToList(block, blockChainFromApi);
            int rebuildResult = blockStoreService.partialRebuild(blockChainFromApi);
            if (rebuildResult == -1) {
                blockStoreService.fullRebuild(blockClientService.getLastRecordsFromApi(targetListSize));
            }
        } catch (Exception e) {
            LOG.error("Exception while processing new block {}", block, e);
        }
    }

    private void addNewBlockToList(Block newBlock, List<Block> fromApiObj) {
        if (newBlock != null && !fromApiObj.get(0).getTime().equals(newBlock.getTime())) {
            fromApiObj.add(0, newBlock);
        }
    }
}
