package org.mcvly.bitdemo.worker.service;

import org.mcvly.bitdemo.worker.client.dto.Block;

import java.util.List;

public interface BlockClientService {

    void startPolling();

    List<Block> getLastRecordsFromApi(int targetSize);
}
