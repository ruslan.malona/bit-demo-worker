package org.mcvly.bitdemo.worker.service;

import org.mcvly.bitdemo.worker.client.dto.Block;
import reactor.core.publisher.Flux;

public interface BlockPublishService {

    Flux<Block> fetchBlockStream();

    void submitNewBlock(Block block);

}
