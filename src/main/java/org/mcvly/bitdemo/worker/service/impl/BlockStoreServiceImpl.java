package org.mcvly.bitdemo.worker.service.impl;

import org.mcvly.bitdemo.worker.client.dto.Block;
import org.mcvly.bitdemo.worker.service.BlockStoreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;

import static java.util.stream.Collectors.toList;

@Component
public class BlockStoreServiceImpl implements BlockStoreService {

    static final String KEY = "blocks";
    private static final Logger LOG = LoggerFactory.getLogger(BlockStoreServiceImpl.class);
    private static final Comparator<Long> REVERSE_COMP = (o1, o2) -> Long.compare(o2, o1);
    private final RedisTemplate<String, String> redisTemplate;
    private final Integer targetListSize;
    private final Integer blockChainCount;
    private BoundListOperations<String, String> listOps;

    @Autowired
    public BlockStoreServiceImpl(RedisTemplate<String, String> redisTemplate,
                                 @Value("${targetListSize}") Integer targetListSize,
                                 @Value("${blockChainCount}") Integer blockChainCount) {
        this.redisTemplate = redisTemplate;
        this.targetListSize = targetListSize;
        this.blockChainCount = blockChainCount;
    }

    static String encodeBlock(Block b) {
        return b.getTime() + ";" + b.getHeight();
    }

    @PostConstruct
    public void init() {
        listOps = redisTemplate.boundListOps(KEY);
    }

    @Override
    public void fullRebuild(List<Block> blocks) {
        storeNewElementsAndTrimList(prepareForRedis(blocks));
    }

    @Override
    public int partialRebuild(List<Block> blocks) {
        List<Long> fromApi = blocks.stream().map(Block::getTime).collect(toList());
        List<Long> fromRedis = getFromRedis(blockChainCount);
        LOG.debug("From API: {}, form REDIS: {}", fromApi, fromRedis);

        int i = findSubList(fromApi, fromRedis);

        switch (i) {
            case 0:
                LOG.debug("Redis is up-to-date");
                return 0;
            case -1:
                LOG.debug("Redis needs to be fully rebuilt");
                return -1;
            default:
                storeNewElementsAndTrimList(prepareForRedis(blocks.subList(0, i)));
                return i;
        }
    }

    private int findSubList(List<Long> fromApi, List<Long> fromRedis) {
        int i = !fromRedis.isEmpty() ? Collections.indexOfSubList(fromApi, fromRedis) : -1;
        LOG.debug("Index of sublist {}", i);
        return i;
    }

    private String[] prepareForRedis(List<Block> blocks) {
        return blocks.stream().sorted(Comparator.comparingLong(Block::getTime))
                .map(BlockStoreServiceImpl::encodeBlock)
                .toArray(String[]::new);
    }

    private List<Long> getFromRedis(int targetSize) {
        List<String> range = listOps.range(-1 * targetSize, -1);
        return range != null ?
                range.stream().map(this::extractTime).sorted(REVERSE_COMP).collect(toList()) : new ArrayList<>();
    }

    private Long extractTime(String s) {
        return Long.parseLong(s.substring(0, s.indexOf(';')));
    }

    private void storeNewElementsAndTrimList(String[] blocksTimestamps) {
        LOG.info("To save: {}", Arrays.asList(blocksTimestamps));
        Long sizeBefore = listOps.size();
        listOps.rightPushAll(blocksTimestamps);
        if (sizeBefore + blocksTimestamps.length > targetListSize) {
            listOps.trim(sizeBefore + blocksTimestamps.length - targetListSize, sizeBefore + blocksTimestamps.length);
        }
    }
}
