package org.mcvly.bitdemo.worker.client;

import org.mcvly.bitdemo.worker.client.dto.Block;
import org.mcvly.bitdemo.worker.client.dto.BlockRestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.text.MessageFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Component
public class BlockchainDataRestClient {

    private static final Logger LOG = LoggerFactory.getLogger(BlockchainDataRestClient.class);

    private final RestTemplate restTemplate;

    private final String endpoint;

    @Autowired
    public BlockchainDataRestClient(RestTemplate restTemplate,
                                    @Value("${blockchain.api.rest}") String endpoint) {
        this.restTemplate = restTemplate;
        this.endpoint = endpoint;
    }

    public List<Block> getBlockHistory(Instant timestamp) {
        String url = MessageFormat.format(endpoint, timestamp.toEpochMilli());
        ResponseEntity<BlockRestResponse> response = restTemplate.getForEntity(url, BlockRestResponse.class);
        if (HttpStatus.OK.equals(response.getStatusCode())) {
            return response.getBody().getBlocks();
        } else {
            LOG.error("response code not OK: {}", response.getStatusCode());
            return new ArrayList<>();
        }
    }
}
