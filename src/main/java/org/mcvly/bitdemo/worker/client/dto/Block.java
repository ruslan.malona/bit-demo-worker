package org.mcvly.bitdemo.worker.client.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Block implements Serializable {

    private static final long serialVersionUID = 2248058798137249424L;

    private final Integer height;
    private final Long time;

    @JsonCreator
    public Block(@JsonProperty("height") Integer height,
                 @JsonProperty("time") Long time) {
        this.height = height;
        this.time = time;
    }

    public Integer getHeight() {
        return height;
    }

    public Long getTime() {
        return time;
    }

    @Override
    public String toString() {
        return "Block{" +
                "height=" + height +
                ", time=" + time +
                '}';
    }
}
