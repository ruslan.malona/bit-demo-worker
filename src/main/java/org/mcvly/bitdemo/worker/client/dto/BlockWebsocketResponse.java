package org.mcvly.bitdemo.worker.client.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BlockWebsocketResponse {

    private final Block block;

    @JsonCreator
    public BlockWebsocketResponse(@JsonProperty("x") Block block) {
        this.block = block;
    }

    public Block getBlock() {
        return block;
    }
}
