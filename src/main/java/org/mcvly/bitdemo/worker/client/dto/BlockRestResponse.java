package org.mcvly.bitdemo.worker.client.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class BlockRestResponse {

    private final List<Block> blocks;

    @JsonCreator
    public BlockRestResponse(@JsonProperty("blocks") List<Block> blocks) {
        this.blocks = blocks;
    }

    public List<Block> getBlocks() {
        return blocks;
    }
}
