package org.mcvly.bitdemo.worker.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.mcvly.bitdemo.worker.client.dto.BlockWebsocketResponse;
import org.mcvly.bitdemo.worker.service.BlockPublishService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URI;

@Component
public class BlockchainDataWebsocketClient extends WebSocketClient {

    private static final String SUBSCRIBE_BLOCK_OPERATION = "{\"op\":\"blocks_sub\"}";

    private static final Logger LOG = LoggerFactory.getLogger(BlockchainDataWebsocketClient.class);

    private final ObjectMapper mapper;
    private final BlockPublishService blockPublishService;

    @Autowired
    public BlockchainDataWebsocketClient(ObjectMapper mapper,
                                         BlockPublishService blockPublishService,
                                         @Value("${blockchain.api.websocket}") String url) {
        super(URI.create(url));
        this.mapper = mapper;
        this.blockPublishService = blockPublishService;
    }

    public void init() {
        LOG.debug("Connecting..");
        this.connect();
    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        LOG.debug("Server open: {}", serverHandshake);
        send(SUBSCRIBE_BLOCK_OPERATION);
    }

    @Override
    public void onMessage(String s) {
        LOG.debug("Message received: {}", s);
        try {
            BlockWebsocketResponse blockWebsocketResponse = mapper.readValue(s, BlockWebsocketResponse.class);
            blockPublishService.submitNewBlock(blockWebsocketResponse.getBlock());
        } catch (Exception e) {
            LOG.error("Error occurred while retrieving new block", e);
        }
    }

    @Override
    public void onClose(int i, String s, boolean b) {
        LOG.debug("Close event");
    }

    @Override
    public void onError(Exception e) {
        LOG.error("Error occurred", e);
    }
}
