package org.mcvly.bitdemo.worker;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.awaitility.Duration;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mcvly.bitdemo.worker.service.BlockManagingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.awaitility.Awaitility.await;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = { "dev" })
public class BitdemoWorkerIntegrationTests {

    private static final String KEY = "blocks";
    private static final int DAY = 24 * 60 * 60 * 1000;

    @Autowired
    private WsServer webSocketServer;

    @Autowired
    private WebServer webServer;

    @Autowired
    private BlockManagingService blockManagingService;

    private Jedis jedis;

    @Before
    public void setup() {
        long current = System.currentTimeMillis();
        mockSuccessResponse(current, sampleBlocks());
        jedis = new Jedis("localhost", 6379);
        jedis.del(KEY);
    }

    @After
    public void tearDown() throws IOException, InterruptedException {
        webSocketServer.stop();
        jedis.del(KEY);
    }

    @Test
    public void errorWhileReceivingNewBlock() throws InterruptedException {
        serverStarts();
        checkBlocksFromApiSaved();
        checkClientSubscribed();

        mockFailureResponse(System.currentTimeMillis() - DAY);
        newBlockArrives();

        // time to receive error during getting blocks
        Thread.sleep(1000);
        mockSuccessResponse(System.currentTimeMillis() - DAY, sampleBlocks2());

        newBlockArrives();

        checkNewBlockSaved();
    }

    private void mockSuccessResponse(long timestamp, String blocks) {
        webServer.stubFor(get(urlMatching("/api/" + getReg(timestamp)))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(blocks)));
    }

    private void mockFailureResponse(Long timestamp) {
        webServer.stubFor(get(urlMatching("/api/" + getReg(timestamp)))
                .willReturn(aResponse()
                        .withStatus(500)));
    }

    private static String getReg(long l) {
        return String.valueOf(l).substring(0, 8) + "\\d\\d\\d\\d\\d";
    }

    private void checkClientSubscribed() {
        await().until(() -> webSocketServer.clientSubscribed());
    }

    private void newBlockArrives() {
        webSocketServer.sendBlock(newBlock());
    }

    private void checkBlocksFromApiSaved() {
        await().atMost(new Duration(10, TimeUnit.SECONDS)).until(() ->
                Arrays.asList("1521913840;514988", "1521914188;514989", "1521914957;514990")
                        .equals(jedis.lrange(KEY, 0, -1)));
    }

    private void checkNewBlockSaved() {
        await().atMost(new Duration(10, TimeUnit.SECONDS)).until(() ->
                Arrays.asList("1521914188;514989", "1521914957;514990", "1521915321;170359")
                        .equals(jedis.lrange(KEY, 0, -1)));
    }

    private void serverStarts() {
        blockManagingService.start();
    }

    private static String sampleBlocks() {
        return "{ \n" +
                "    \"blocks\" : [\n" +
                "    {\n" +
                "        \"height\" : 514990,\n" +
                "        \"hash\" : \"0000000000000000004312b5544671c8c0ee37b25758e93858877de8bb3b51ff\",\n" +
                "        \"time\" : 1521914957,\n" +
                "        \"main_chain\" : true\n" +
                "    },\n" +
                "    \n" +
                "    {\n" +
                "        \"height\" : 514989,\n" +
                "        \"hash\" : \"0000000000000000000884ad62c7036a7e2022bca3f0bd68628414150e8a0ea6\",\n" +
                "        \"time\" : 1521914188,\n" +
                "        \"main_chain\" : true\n" +
                "    },\n" +
                "    \n" +
                "    {\n" +
                "        \"height\" : 514988,\n" +
                "        \"hash\" : \"0000000000000000001898436687fc920f124eba5dceead79b6ba5a2a3c9193c\",\n" +
                "        \"time\" : 1521913840,\n" +
                "        \"main_chain\" : true\n" +
                "    }\n" +
                "    ]\n" +
                "}";
    }

    private static String sampleBlocks2() {
        return "{ \n" +
                "    \"blocks\" : [\n" +
                "    {\n" +
                "        \"height\" : 514737,\n" +
                "        \"hash\" : \"0000000000000000003b2f13a1ea65c9ab8ed118b40de17aa39348f5c6fb44e8\",\n" +
                "        \"time\" : 1521763203,\n" +
                "        \"main_chain\" : true\n" +
                "    },\n" +
                "    \n" +
                "    {\n" +
                "        \"height\" : 514738,\n" +
                "        \"hash\" : \"00000000000000000032651a4e19688e1a7f02b872113228c6c17285e27d7ce7\",\n" +
                "        \"time\" : 1521763224,\n" +
                "        \"main_chain\" : true\n" +
                "    }\n" +
                "    ]\n" +
                "}";
    }


    private static String newBlock() {
        return "{\n" +
                "    \"op\": \"block\",\n" +
                "    \"x\": {\n" +
                "        \"txIndexes\": [\n" +
                "            3187871,\n" +
                "            3187868\n" +
                "        ],\n" +
                "        \"nTx\": 0,\n" +
                "        \"totalBTCSent\": 0,\n" +
                "        \"estimatedBTCSent\": 0,\n" +
                "        \"reward\": 0,\n" +
                "        \"size\": 0,\n" +
                "        \"blockIndex\": 190460,\n" +
                "        \"prevBlockIndex\": 190457,\n" +
                "        \"height\": 170359,\n" +
                "        \"hash\": \"00000000000006436073c07dfa188a8fa54fefadf571fd774863cda1b884b90f\",\n" +
                "        \"mrklRoot\": \"94e51495e0e8a0c3b78dac1220b2f35ceda8799b0a20cfa68601ed28126cfcc2\",\n" +
                "        \"version\": 1,\n" +
                "        \"time\": 1521915321,\n" +
                "        \"bits\": 436942092,\n" +
                "        \"nonce\": 758889471\n" +
                "    }\n" +
                "}";
    }

    @TestConfiguration
    static class TestConfig {

        @Bean
        public WsServer webSocketServer() {
            WsServer wsServer = new WsServer(new InetSocketAddress(8887));
            wsServer.start();
            return wsServer;
        }

        @Bean
        public WebServer webServer() {
            WebServer webServer = new WebServer();
            webServer.start();
            return webServer;
        }
    }

    private static class WebServer extends WireMockServer {
        public WebServer() {
            super(wireMockConfig().port(8089));
        }
    }

    private static class WsServer extends WebSocketServer {
        final Logger LOG = LoggerFactory.getLogger(WsServer.class);
        private volatile WebSocket client;

        public WsServer(InetSocketAddress address) {
            super(address);
        }

        @Override
        public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
            LOG.info("Websocket server new connection {}", clientHandshake.getResourceDescriptor());
        }

        @Override
        public void onClose(WebSocket webSocket, int i, String s, boolean b) {

        }

        @Override
        public void onMessage(WebSocket webSocket, String s) {
            LOG.info("Websocket server received message {}", s);
            this.client = webSocket;
        }

        @Override
        public void onError(WebSocket webSocket, Exception e) {

        }

        @Override
        public void onStart() {
            LOG.info("Websocket server started");
        }

        public void sendBlock(String block) {
            LOG.info("Sending new block");
            this.client.send(block);
        }

        public boolean clientSubscribed() {
            return client != null;
        }
    }
}
