package org.mcvly.bitdemo.worker.client

import org.mcvly.bitdemo.worker.client.dto.Block
import org.mcvly.bitdemo.worker.client.dto.BlockRestResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

import java.time.Instant

class BlockchainDataRestClientSpec extends Specification {

    def restTemplate = Mock(RestTemplate)
    def instant = Instant.now()
    def url = "https://blockchain.info/blocks/${instant.toEpochMilli()}?format=json"
    def client = new BlockchainDataRestClient(restTemplate, url)

    def "successfully gets blocks"() {
        def blocks = [Mock(Block)]
        def response = new BlockRestResponse(blocks)

        when:
        def res = client.getBlockHistory(instant)

        then:
        1 * restTemplate.getForEntity(url, BlockRestResponse.class) >> new ResponseEntity<>(response, HttpStatus.OK)
        res == blocks
    }

    def "internal error from API"() {
        when:
        def res = client.getBlockHistory(instant)

        then:
        1 * restTemplate.getForEntity(url, BlockRestResponse.class) >> new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR)
        res == []
    }

}
