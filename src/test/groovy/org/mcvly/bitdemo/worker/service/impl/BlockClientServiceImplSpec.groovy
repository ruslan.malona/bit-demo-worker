package org.mcvly.bitdemo.worker.service.impl

import org.mcvly.bitdemo.worker.client.BlockchainDataRestClient
import org.mcvly.bitdemo.worker.client.BlockchainDataWebsocketClient
import org.mcvly.bitdemo.worker.client.dto.Block
import spock.lang.Specification

import java.time.Instant

class BlockClientServiceImplSpec extends Specification {

    def restClient = Mock(BlockchainDataRestClient)
    def websockClient = Mock(BlockchainDataWebsocketClient)
    def service = new BlockClientServiceImpl(restClient, websockClient)

    def "get records from API"() {
        def block1 = new Block(1, 1)
        def block2 = new Block(2, 2)

        when:
        def res = service.getLastRecordsFromApi(2)

        then:
        1 * restClient.getBlockHistory({it.toEpochMilli() > (System.currentTimeMillis() - 1000)} as Instant) >> [block1]
        1 * restClient.getBlockHistory({it.toEpochMilli() < (System.currentTimeMillis() - 1000)} as Instant) >> [block2]
        res == [block2, block1]
    }

}
