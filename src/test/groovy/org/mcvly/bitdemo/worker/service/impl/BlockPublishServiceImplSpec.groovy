package org.mcvly.bitdemo.worker.service.impl

import org.mcvly.bitdemo.worker.client.dto.Block
import spock.lang.Specification

import java.util.concurrent.CountDownLatch
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


class BlockPublishServiceImplSpec extends Specification {

    def service = new BlockPublishServiceImpl()

    def "publish and consume block"() {

        def startSignal = new CountDownLatch(1)
        def pool = Executors.newCachedThreadPool()

        when:
        pool.submit({
            service.fetchBlockStream().doOnNext({data ->
                println "Received new block $data.height"
                startSignal.countDown()
            }).subscribe()
        } as Runnable)
        service.submitNewBlock(new Block(1, 2))

        then:
        startSignal.await(2, TimeUnit.SECONDS)
    }
}