package org.mcvly.bitdemo.worker.service

import org.mcvly.bitdemo.worker.client.dto.Block
import reactor.core.publisher.Flux
import spock.lang.Specification

class BlockManagingServiceSpec extends Specification {

    def blockClientService = Mock(BlockClientService)
    def blockStoreService = Mock(BlockStoreService)

    def producer = Mock(Flux)
    def blockPublishService = Mock(BlockPublishService)

    def managingService = new BlockManagingService(blockClientService, blockStoreService, blockPublishService, 100, 6)

    def "invoke init method"() {
        def blocks = [Mock(Block)]
        when:
        managingService.start()

        then:
        1 * blockClientService.getLastRecordsFromApi(100) >> blocks
        1 * blockStoreService.fullRebuild(blocks)
        1 * blockPublishService.fetchBlockStream() >> producer
        1 * producer.subscribe(_)
        1 * blockClientService.startPolling()
    }

    def "new block successfully added"() {
        def block = new Block(2, 2)
        def apiBlocks = [new Block(1, 1)]

        when:
        managingService.newBlockReceived(block)

        then:
        1 * blockClientService.getLastRecordsFromApi(12) >> apiBlocks
        1 * blockStoreService.partialRebuild([block] + apiBlocks) >> 1
    }

    def "block not added, need full rebuild"() {
        def block = new Block(2, 2)
        def apiBlocks = [new Block(1, 1)]

        when:
        managingService.newBlockReceived(block)

        then:
        1 * blockClientService.getLastRecordsFromApi(12) >> apiBlocks
        1 * blockStoreService.partialRebuild([block] + apiBlocks) >> -1
        1 * blockClientService.getLastRecordsFromApi(100) >> apiBlocks
        1 * blockStoreService.fullRebuild(apiBlocks)
    }

}