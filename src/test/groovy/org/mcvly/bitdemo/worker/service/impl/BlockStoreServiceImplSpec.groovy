package org.mcvly.bitdemo.worker.service.impl

import org.mcvly.bitdemo.worker.client.dto.Block
import org.springframework.data.redis.core.BoundListOperations
import org.springframework.data.redis.core.RedisTemplate
import spock.lang.Specification

import static org.mcvly.bitdemo.worker.service.impl.BlockStoreServiceImpl.encodeBlock

class BlockStoreServiceImplSpec extends Specification {

    def targetSize = 3
    def blockchainSize = 2

    def listOps = Mock(BoundListOperations)
    def redisTemplate = Mock(RedisTemplate)

    def service = new BlockStoreServiceImpl(redisTemplate, targetSize, blockchainSize)

    def setup() {
        redisTemplate.boundListOps(BlockStoreServiceImpl.KEY) >> listOps
        service.init()
    }

    def "store initial"() {
        def block1 = new Block(1, 101)
        def block2 = new Block(2, 202)
        def block3 = new Block(3, 303)

        when:
        service.fullRebuild([block1, block2, block3])

        then:
        1 * listOps.size() >> 100
        1 * listOps.rightPushAll([encodeBlock(block1), encodeBlock(block2), encodeBlock(block3)])
        1 * listOps.trim(100, 103)
    }

    def "redis contains all new elements, nothing to do"() {
        def apiBlock1 = new Block(2, 202)
        def apiBLock2 = new Block(1, 101)

        when:
        def res = service.partialRebuild([apiBlock1, apiBLock2])

        then:
        1 * listOps.range(-2, -1) >> [encodeBlock(apiBlock1), encodeBlock(apiBLock2)]
        0 * listOps._
        res == 0
    }

    def "sublist not found in redis, need full rebuild"() {
        def newBlock = new Block(3, 303)
        def apiBlock1 = new Block(2, 202)
        def apiBLock2 = new Block(1, 101)

        when:
        def res = service.partialRebuild([newBlock, apiBlock1, apiBLock2])

        then:
        1 * listOps.range(-2, -1) >> [encodeBlock(new Block(8, 808)), encodeBlock(new Block(9, 909))]
        0 * listOps._
        res == -1
    }

    def "store one new element without removing old"() {
        def newBlock = new Block(3, 303)
        def apiBlock1 = new Block(2, 202)
        def apiBLock2 = new Block(1, 101)

        when:
        def res = service.partialRebuild([newBlock, apiBlock1, apiBLock2])

        then:
        1 * listOps.range(-2, -1) >> [encodeBlock(apiBLock2), encodeBlock(apiBlock1)]
        1 * listOps.size() >> 2
        1 * listOps.rightPushAll([encodeBlock(newBlock)])
        res == 1
    }

    def "store one new element and remove old"() {
        def newBlock = new Block(3, 303)
        def apiBlock1 = new Block(2, 202)
        def apiBLock2 = new Block(1,101)

        when:
        def res = service.partialRebuild([newBlock, apiBlock1, apiBLock2])

        then:
        1 * listOps.range(-2, -1) >> [encodeBlock(apiBLock2), encodeBlock(apiBlock1)]
        1 * listOps.size() >> 3
        1 * listOps.rightPushAll([encodeBlock(newBlock)])
        1 * listOps.trim(1, 4)
        res == 1
    }

    def "store two new elements and remove old"() {
        def newBlock = new Block(3,303)
        def apiBlock1 = new Block(2, 202)
        def apiBLock2 = new Block(1,101)
        def apiBLock3 = new Block(0, 0)

        when:
        def res = service.partialRebuild([newBlock, apiBlock1, apiBLock2, apiBLock3])

        then:
        1 * listOps.range(-2, -1) >> [encodeBlock(apiBLock3), encodeBlock(apiBLock2)]
        1 * listOps.size() >> 3
        1 * listOps.rightPushAll([encodeBlock(apiBlock1), encodeBlock(newBlock)])
        1 * listOps.trim(2, 5)
        res == 2
    }
}